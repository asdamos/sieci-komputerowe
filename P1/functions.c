//Adam Sawicki
//270814


#include "functions.h"
#include <netinet/ip.h>
u_int16_t compute_icmp_checksum (const void *buff, int length)
{
    u_int32_t sum;
    const u_int16_t* ptr = buff;
    assert (length % 2 == 0);
    for (sum = 0; length > 0; length -= 2)
        sum += *ptr++;
    sum = (sum >> 16) + (sum & 0xffff);
    return (u_int16_t)(~(sum + (sum >> 16)));
}

void err_sys(const char* x)
{
    perror(x);
    exit(1);
}

void Gettimeofday(struct timeval *tv, void *foo)
{
    if (gettimeofday(tv, foo) == -1)
        err_sys("Gettimeofday error");
    return;
}

/*
Returns:    -1 if no packets to receive or some error on packet
            -2 if ICMP echo reply
            -3 if ICMP time exceeded
*/
int receive(int ttl)
{
    struct sockaddr_in sender;
    socklen_t sender_len = sizeof(sender);
    u_int8_t buffer[IP_MAXPACKET+1];
    int n;

    struct timeval rec_tv;
    n = recvfrom(sockfd, buffer, IP_MAXPACKET, MSG_DONTWAIT, (struct sockaddr*)&sender, &sender_len);
    Gettimeofday(&rec_tv, NULL);
    if(n == -1)
    {
        if(errno == EWOULDBLOCK)
        {
            return -1;
        }
        else
        {
            err_sys("Recvfrom error");
        }
    }

    struct iphdr *ip_hdr;
    int ip_hlen;
    ip_hdr = (struct iphdr*) buffer; //start of IP header
    ip_hlen = (ip_hdr->ihl) * 4;

    struct icmphdr *icmp_hdr;
    unsigned int icmp_len;
    icmp_hdr = (struct icmphdr*) (buffer + ip_hlen);   //start of ICMP header

    if((icmp_len = n-ip_hlen) < 8) //icmp header is too short
        return -1;

    //Packet is TIME EXCEEDED type
    if(icmp_hdr->type == ICMP_TIME_EXCEEDED && icmp_hdr->code == ICMP_EXC_TTL)
    {
        
        if(icmp_len < 8 + sizeof(struct iphdr)) //not enough data to look at inner IP
            return -1;

        struct iphdr *hip;
        hip = (struct iphdr*) (buffer + ip_hlen + 8);  //icmp header is 8 bytes long
        unsigned int ip_hlen2 = (hip->ihl) * 4;

        if(icmp_len < 8 + ip_hlen2 + 8)    //not enought data to look at inner ICMP
            return -1;

        if(hip->protocol == IPPROTO_ICMP)
        {
            
            struct icmphdr *orig;
            orig = (struct icmphdr*) (buffer + ip_hlen + 8 + ip_hlen2); //8 bytes for first ICMP header

            pid_t id = ntohs(orig->un.echo.id);
            int seq = ntohs(orig->un.echo.sequence);

            
            if(id != pid) //not our packet
            {
                return -1;
            }
            

            int ttl_of_packet = ((seq-1) / nprobes) + 1; //1,2,3..
            
            if(ttl_of_packet < ttl) //packet with old TTL
            {
                
                return -1;
            }
            int probe_number = (seq -1) % nprobes;
            rec_tvs[probe_number].tv_sec = rec_tv.tv_sec;
            rec_tvs[probe_number].tv_usec = rec_tv.tv_usec;


            received[probe_number] = true;


            inet_ntop(AF_INET, &(sender.sin_addr), ip_adress[probe_number], sizeof(ip_adress[probe_number]));

            return -3;
        }
        else
        {
            return -1;
        }
    }

    if(icmp_hdr->type == ICMP_ECHOREPLY)
    {
        
        pid_t id = ntohs(icmp_hdr->un.echo.id);
        int seq = ntohs(icmp_hdr->un.echo.sequence);

        if(id != pid) //not our packet
        {
            return -1;
        }
       

        int ttl_of_packet = ((seq-1) / nprobes) + 1; //1,2,3..
        
        if(ttl_of_packet < ttl) //packet with old TTL
        {
            
            return -1;
        }
        int probe_number = (seq -1) % nprobes;
        rec_tvs[probe_number].tv_sec = rec_tv.tv_sec;
        rec_tvs[probe_number].tv_usec = rec_tv.tv_usec;

        received[probe_number] = true;

        inet_ntop(AF_INET, &(sender.sin_addr), ip_adress[probe_number], sizeof(ip_adress[probe_number]));

        return -2;
    }

    return 0;
}

u_int64_t time_diff_in_ms(struct timeval start, struct timeval end)
{
    return (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_usec - start.tv_usec) / 1000;
}


//function to print answers after receiving packets
void print_ans(int rec_number, struct timeval sen_tv[])
{
    if(rec_number == 0)
    {
        printf("*\n");
        return;
    }
    bool received_all = true;
    for(int i=0; i<nprobes; i++)
    {
        if(!received[i])
        {
            received_all = false;
        }
    }
    u_int64_t time = 0;
    //compute time
    if(received_all)
    {
        for(int i=0; i<nprobes; i++)
        {
            time += time_diff_in_ms(sen_tv[i], rec_tvs[i]);
        }
        time /= nprobes;
    }

    int printed = 0;
    char printed_adresses[nprobes][16];

    //print all ip adresses
    for(int i = 0; i<nprobes; i++)
    {
        bool was_printed = false;
        for(int j = 0; j<printed; j++)
        {
            if(received[i])
            {
                if(strcmp(ip_adress[i], printed_adresses[j]) == 0)  //we printed it before
                {

                    was_printed = true;
                }
            }

        }
        if(!was_printed && received[i])
        {
            printf("%s ", ip_adress[i]);
            strcpy(printed_adresses[printed], ip_adress[i]);
            printed++;
        }
    }

    if(received_all)
    {
        printf("%" PRIu64 "ms\n", time);
    }
    else
    {
        printf("???\n");
    }

}
