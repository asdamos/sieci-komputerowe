//Adam Sawicki
//270814


#ifndef _functions_h_
#define _functions_h_


#include <sys/types.h>
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <errno.h>
#include <netinet/ip_icmp.h>
#include <arpa/inet.h>
#include <stdint.h>

#include <netinet/in.h>

#include <netinet/ip.h>

#include <netinet/udp.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <net/if.h>
#include <inttypes.h>

extern int sockfd; //socket file descriptor
extern pid_t pid;  //our process id
extern int max_ttl, nprobes;
extern char ip_adress[3][16];
extern struct timeval rec_tvs[3];
extern bool received[3];

u_int16_t compute_icmp_checksum (const void *buff, int length);


void err_sys(const char* x);

void Gettimeofday(struct timeval *tv, void *foo);

int receive(int ttl);

u_int64_t time_diff_in_ms(struct timeval start, struct timeval end);

void print_ans(int rec_number, struct timeval sen_tv[]);


#endif // _functions_h_
