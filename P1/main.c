//Adam Sawicki
//270814

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <strings.h>
#include <sys/time.h>
#include <netinet/ip_icmp.h>
#include "functions.h"

    int sockfd; //socket file descriptor
    pid_t pid;  //our process id
    int max_ttl = 30, nprobes = 3;
    int ttl, probe, seq = 1;

    unsigned int waiting_time = 1000; //1000ms

    struct timeval rec_tvs[3];
    char ip_adress[3][16];
    bool received[3];


int main(int argc, char *argv[])
{
    if(argc != 2)
    {
        err_sys("Wrong argument number");
    }

    //Generate host adress
    struct sockaddr_in recipient;
    bzero(&recipient, sizeof(recipient));
    recipient.sin_family = AF_INET;
    if(inet_pton(AF_INET, argv[1], &recipient.sin_addr) == 0) //returns 0 if IP adress is not correct
    {
        err_sys("Wrong ip adress");
    }

    sockfd = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP); //raw socket with ICMP protocol
    if(sockfd == -1)
    {
        err_sys("Error getting socket file descriptor");
    }
    pid = getpid();

    bool done = false;

    for(ttl = 1; ttl<= max_ttl && !done; ttl++)
    {
        if(setsockopt(sockfd, IPPROTO_IP, IP_TTL, &ttl, sizeof(int)) == -1)
        {
            err_sys("Setsockopt error");
        }

        printf("%2d. ", ttl);

        struct timeval sen_tv[nprobes];   //timevals of sended packets

        //send n probes
        for(probe = 0; probe < nprobes; probe++)
        {
            struct icmphdr icmp_header;
            icmp_header.type = ICMP_ECHO;
            icmp_header.code = 0;
            icmp_header.un.echo.id = htons(pid);
            icmp_header.un.echo.sequence = htons(seq);
            icmp_header.checksum = 0;
            icmp_header.checksum = compute_icmp_checksum((u_int16_t*)&icmp_header, sizeof(icmp_header));
            seq++;

            if(sendto(sockfd, &icmp_header, sizeof(icmp_header), 0, (struct sockaddr*)&recipient, sizeof(recipient)) == -1)
            {
                err_sys("Sendto error");
            }
            Gettimeofday(&sen_tv[probe], NULL);
        }
        received[0] = received[1] = received[2] = false;

        //start of waiting
        struct timeval start_tv, tmp_tv;
        Gettimeofday(&start_tv, NULL);
        u_int64_t time_diff;


        int received_number = 0;

        do
        {   int code = receive(ttl);

            if(code == -3)//packet TIME EXCEEDED
            {
                received_number++;
            }
            else if(code == -2) //echo reply, end
            {

                done = true;
                received_number++;
            }

            Gettimeofday(&tmp_tv, NULL);
            time_diff= time_diff_in_ms(start_tv, tmp_tv);

        }while(time_diff < waiting_time && received_number<3);

        print_ans(received_number, sen_tv);

    }

    return 0;
}
