//Adam Sawicki
//270814


#include "DistanceVector.h"

DistanceVector::DistanceVector()
{
    //ctor
}

DistanceVector::~DistanceVector()
{
    //dtor
}


DistanceVector::DistanceVector(Network *init, int sockfdc)
{
    n = init->n;
    round = 0;
    my_net = init;
    sockfd = sockfdc;
    for(int i=0; i<n; i++)
    {
        ip_bits.push_back(init->networks[i]);
        mask_bits.push_back(init->netmask_bits[i]);
        distance.push_back(inf);
        via.push_back(0);
        connected_directly.push_back(false);
        last_round.push_back(round);



    }
}


void DistanceVector::analyseMessage(int length, char * buffer, sockaddr_in senderaddr)
{
    char * ip = inet_ntoa(senderaddr.sin_addr);
    unsigned int a,b,c,d, ipbit;
    sscanf(ip, "%d.%d.%d.%d", &a, &b, &c, &d);
    ipbit = make_bit_addr(a,b,c,d);


	//printf("odebralem od %d %d %d %d\n", a,b,c,d);
    //find network of sender
    unsigned int network_sender, mask_sender, distance_sender;
    for(int i=0; i<my_net->n; i++)
    {
	if(ipbit == my_net->my_ips_bits[i]) //our own message
	{
	    return;
	}
        unsigned int temp;
        temp = ipbit & my_net->netmask_bits[i];
        if(temp == my_net->networks[i])
        {
            network_sender = temp;
            mask_sender = my_net->netmask_bits[i];
            distance_sender = my_net->distances[i];
            break;
        }
    }

    if(length != 12)
    {
        fprintf(stderr, "Dlugosc rozna od 12\n");
    }
    else
    {
        unsigned int ip_rc, mask_rc, dist_rc;

        ip_rc = (unsigned int&) *buffer;
        mask_rc = (unsigned int&) *(buffer+4);
        dist_rc = (unsigned int&) *(buffer+8);

        bool exist_in_vector = false;
        {
            for(int i=0; i<this->n; i++)
            {
                if(ip_bits[i] == ip_rc && mask_rc == mask_bits[i])     //we have this router!
                {
                    if(dist_rc >= inf && via[i] == ipbit) //distance is inf and we go by this router
                    {
                        if(distance[i] < inf)
                            last_round[i] = round;
                        distance[i] = inf;
                        via[i] = 0;
                        connected_directly[i] = false;
                        //last_round[i] = round;
                    }
                    else if(dist_rc + distance_sender < distance[i])
                    {
                        distance[i] = dist_rc + distance_sender;
                        connected_directly[i] = false;
                        last_round[i] = round;
                        via[i] = ipbit;
                    }
                    else if(via[i] == ipbit && distance[i] < inf)
                    {
                        distance[i] = dist_rc + distance_sender;
                        last_round[i] = round;
                    }
                    exist_in_vector = true;
                    break;
                }
            }
        }
        if(!exist_in_vector && dist_rc < inf)
        {
            char charip[16];
            bit_ip_to_char(ip_rc, charip);
            //printf("Dodalem %s\n", charip);
            ip_bits.push_back(ip_rc);
            mask_bits.push_back(mask_rc);
            distance.push_back(dist_rc + distance_sender);
            via.push_back(ipbit);
            connected_directly.push_back(false);
            last_round.push_back(round);
            this->n++;
        }

    }


    //check for our sender network
    bool found = false;
    for(int i=0; i<this->n; i++)
    {
        if(network_sender == ip_bits[i] && mask_bits[i] == mask_sender)
        {
            found = true;
            if(distance[i] > distance_sender)
            {
                distance[i] = distance_sender;
                connected_directly[i] = true;
                last_round[i] = round;
            }
        }
    }

    if(!found)
    {
        //printf("Dodajemy nadawce\n");
        ip_bits.push_back(network_sender);
        mask_bits.push_back(mask_sender);
        distance.push_back(distance_sender);
        via.push_back(0);
        connected_directly.push_back(true);
        last_round.push_back(round);
        this->n++;
    }
}


void DistanceVector::send()
{
    for(int i=0; i<my_net->n; i++)
    {
        char bc_ip[16];
        unsigned int bit_adr = my_net->broadcasts[i];
        bit_ip_to_char(bit_adr, bc_ip);
        struct sockaddr_in dest;
        bzero(&dest, sizeof(dest));
        dest.sin_family = AF_INET;
        dest.sin_port = htons(PORT);
        inet_pton(AF_INET, bc_ip, &dest.sin_addr);
        //printf("wyslalem do %s\n", bc_ip);
        for(int j=0; j<this->n; j++)    //send information about all networks
        {
            char msg[12];   //we send 3 4 bytes uints, ip, mask and distance
            memcpy(msg, &ip_bits[j], 4);
            memcpy(msg+4, &mask_bits[j], 4);
            memcpy(msg+8, &distance[j], 4);




            if(-1 == sendto(sockfd, msg, 12, 0, (struct sockaddr*) &dest, sizeof(dest)))
            {
                //we cannot send to this broadcast adress, set it distance to inf
                for(int k=0; k<this->n; k++)
                {
                    if(ip_bits[k] == bit_adr)
                    {
                        if(distance[k] != inf)
                        {
                            distance[k] = inf;
                            via[k]=0;
                            connected_directly[k]=false;
                            last_round[k] = round;
                        }
                    }
                }
            }
        }
    }
}

void DistanceVector::print()
{
    for(int i=0; i<n; i++)
    {
        char ip[16];
        bit_ip_to_char(ip_bits[i], ip);
        int mask = bit_mask_to_int(mask_bits[i]);
        printf("%s/%d ", ip, mask);

        if(distance[i] >= inf)
        {
            printf("unreachable\n");
        }
        else
        {
            printf("distance %d ", distance[i]);

            if(connected_directly[i])
            {
                printf("connected directly\n");
            }
            else
            {
                bit_ip_to_char(via[i], ip);
                printf("via %s\n", ip);
            }
        }
    }
    printf("\n");
}


void DistanceVector::testRoundTimeout()
{
    //first check for no messages from via routers without inf distance
    for(int i=0; i<this->n; i++)
    {
        if((round - last_round[i] > round_timeout) && distance[i] < inf)
        {
            distance[i] = inf;
            last_round[i] = round;
        }
    }

    for(int i=0; i<this->n; i++)
    {
        if((round - last_round[i] > round_timeout) && distance[i] >= inf)
        {
            ip_bits.erase(ip_bits.begin()+i);
            mask_bits.erase(mask_bits.begin()+i);
            distance.erase(distance.begin()+i);
            via.erase(via.begin()+i);
            connected_directly.erase(connected_directly.begin()+i);
            last_round.erase(last_round.begin()+i);
            this->n--;
        }
    }
}
