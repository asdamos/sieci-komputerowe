//Adam Sawicki
//270814

#ifndef DISTANCEVECTOR_H
#define DISTANCEVECTOR_H

#include <arpa/inet.h>
#include <netinet/udp.h>
#include "Network.h"
#include <vector>
#include <thread>
#include <mutex>
#include <strings.h>
#include <string.h>

using namespace std;

extern int PORT;
extern unsigned int inf;
extern int round_timeout;

class DistanceVector
{
    public:
        DistanceVector();
        DistanceVector(Network * init, int sockfd);
        ~DistanceVector();

        mutex mtx;

        Network * my_net;

        int n; //numbers of networks in our model
        int round;
        int sockfd;
        vector<unsigned int>ip_bits;
        vector<unsigned int>mask_bits;
        vector<unsigned int> distance;
        vector<unsigned int> via;
        vector<bool> connected_directly;
        vector<int> last_round;


        void analyseMessage(int length, char * buffer, struct sockaddr_in senderaddr);

        void send();
        void print();

        void testRoundTimeout();
};

#endif // DISTANCEVECTOR_H
