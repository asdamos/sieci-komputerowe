//Adam Sawicki
//270814

#include "Network.h"

Network::Network()
{

    scanf("%d", &n);

    my_ips = new char*[n];

    netmasks = new int[n];
    distances = new unsigned int[n];

    my_ips_bits = new unsigned int[n];
    netmask_bits = new unsigned int[n];

    networks = new unsigned int[n];
    broadcasts = new unsigned int[n];

    for(int i=0; i<n; i++)
    {
        my_ips[i] = new char[16];
        unsigned int a, b, c, d;
        scanf("%s netmask /%d distance %d", my_ips[i], &netmasks[i], &distances[i]);
        sscanf(my_ips[i], "%d.%d.%d.%d", &a, &b, &c, &d);
        my_ips_bits[i] = make_bit_addr(a,b,c,d);
        netmask_bits[i] = make_bit_netmask(netmasks[i]);

        networks[i] = my_ips_bits[i] & netmask_bits[i];
        broadcasts[i] = networks[i] | (~netmask_bits[i]);

    }
}

Network::~Network()
{
    for(int i=0; i<n; i++)
    {
        delete my_ips[i];
    }
    delete my_ips;
    delete distances;
    delete my_ips_bits;
    delete netmask_bits;
}
