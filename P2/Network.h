//Adam Sawicki
//270814

#ifndef NETWORK_H
#define NETWORK_H

#include <cstdio>
#include "functions.h"
#include <mutex>



class Network
{
    public:
        Network();
        ~Network();

        std::mutex mtx;

        int n;
        char **my_ips;
        int *netmasks;
        unsigned int *distances;

        unsigned int *my_ips_bits;
        unsigned int *netmask_bits;
        unsigned int *networks;
        unsigned int *broadcasts;

    private:
};

#endif // NETWORK_H
