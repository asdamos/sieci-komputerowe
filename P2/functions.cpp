//Adam Sawicki
//270814


#include "functions.h"




unsigned int make_bit_addr(unsigned int a, unsigned int b, unsigned int c, unsigned int d)
{
    unsigned int res = 0;
    res |= a;
    res <<= 8;
    res |= b;
    res <<= 8;
    res |= c;
    res <<= 8;
    res |= d;
    return res;
}



unsigned int make_bit_netmask(int n)
{
    unsigned int res = 0;
    res = ~res;
    res = res << (32 - n);
    return res;
}



void bit_ip_to_char(unsigned int ip, char * res)
{
    unsigned int magic = 0x000000FF;
    unsigned int a,b,c,d;
    d = ip & magic;
    ip >>= 8;
    c = ip & magic;
    ip >>= 8;
    b = ip & magic;
    ip >>= 8;
    a = ip & magic;

    sprintf(res, "%d.%d.%d.%d", a,b,c,d);
}


int bit_mask_to_int(unsigned int mask)
{
    int res;
    for (res = 0; mask; mask >>= 1)
    {
        res += mask & 1;
    }
    return res;
}

