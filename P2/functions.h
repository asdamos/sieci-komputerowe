//Adam Sawicki
//270814

#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#define MAXLINE 1024
#include <cstdio>


unsigned int make_bit_addr(unsigned int a, unsigned int b, unsigned int c, unsigned int d);

unsigned int make_bit_netmask(int n);

void bit_ip_to_char(unsigned int ip, char * res);

int bit_mask_to_int(unsigned int mask);

#endif
