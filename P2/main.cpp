//Adam Sawicki
//270814

#include <arpa/inet.h>
#include <netinet/udp.h>
#include <cstdio>
#include "Network.h"
#include "DistanceVector.h"
#include <cstdlib>
#include <strings.h>
#include "functions.h"
#include <string.h>

#include <thread>
#include <mutex>
#include <chrono>

using namespace std;

Network * my_net;
DistanceVector *my_vector;

int PORT = 15000;
unsigned int inf = 32;
int round_time = 10; //in seconds
int round_timeout = 2;  //rounds
void sending_thread();


int main()
{
    int sockfd;
    struct sockaddr_in servaddr, senderaddr;

    sockfd = socket(AF_INET, SOCK_DGRAM, 0);

	int broadcast_perm = 1;
setsockopt (sockfd, SOL_SOCKET, SO_BROADCAST, (void *)&broadcast_perm, sizeof(broadcast_perm));

    if(sockfd == -1)
        exit(1);

    my_net = new Network();
    my_vector = new DistanceVector(my_net, sockfd);


    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(PORT);

    if(-1 == bind(sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr)))
        exit(2);

    ssize_t received_bytes;
    socklen_t len;
    char buffer[MAXLINE];

    thread sendthread(sending_thread);

    while(true)
    {
        len = sizeof(senderaddr);
	//printf("czekam na odbior\n");
        received_bytes = recvfrom(sockfd, buffer, MAXLINE, 0, (struct sockaddr*)&senderaddr, &len);

        if(received_bytes == -1)
            exit(3);
	//printf("odebralem!\n");
        my_vector->mtx.lock();
        my_vector->analyseMessage(received_bytes, buffer, senderaddr);
        my_vector->mtx.unlock();
 }
    return 0;
}


void sending_thread()
{
    while(true)
    {
        my_vector->mtx.lock();
	//printf("wyslalem\n");
        my_vector->send();
        my_vector->print();
        my_vector->testRoundTimeout();
        my_vector->round++;
        my_vector->mtx.unlock();
        this_thread::sleep_for(chrono::seconds(round_time));
    }

}
