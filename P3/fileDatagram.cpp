//Adam Sawicki
//270814
#include "fileDatagram.hpp"

fileDatagram::fileDatagram(char * f_n, int  f_s, int sfd, int w_s, int p_s, char * ip_address, int serv_port, int p_t)
{
    file_name = f_n;
    file_size = f_s;
    sockfd = sfd;

    window_size = w_s;
    packet_size = p_s;
    packet_timeout = p_t;

    packet_number = file_size / packet_size;

    if(file_size % packet_size > 0)
        packet_number++;

    packets = new struct packet[packet_number];

    for(int i=0; i<packet_number; i++)
    {
        packets[i].confirmed = false;
        packets[i].mailed = false;
    }
    window_iter = 0;
    port = serv_port;
    strcpy(ip, ip_address);

}

fileDatagram::~fileDatagram()

{
    //dtor
}


void fileDatagram::send()
{

    struct timeval now;
    for(int i=window_iter; (i < packet_number) && (i <window_iter + window_size) ; i++)
    {

        if(packets[i].confirmed == false)
        {
            Gettimeofday(&now, NULL);
            if(time_diff_in_ms(packets[i].send_time, now) > (unsigned int)packet_timeout || packets[i].mailed == false)
            {

                packets[i].mailed = true;
                Gettimeofday(&packets[i].send_time, NULL);
                char message[50];
                int start = i * packet_size;
                int end = packet_size;
                if(start + packet_size > file_size)
                {
                    end = file_size % packet_size;
                }

                sprintf(message, "GET %d %d\n", start, end);
                ssize_t message_len = strlen(message);


                struct sockaddr_in server_address;
                memset(&server_address, 0, sizeof(server_address));

                server_address.sin_family = AF_INET;
                server_address.sin_addr.s_addr = inet_addr(ip);
                server_address.sin_port = htons(port);

                if (sendto(sockfd, message, message_len, 0, (struct sockaddr*) &server_address, sizeof(server_address)) != message_len)
                {
                    fprintf(stderr, "sendto error: %s\n", strerror(errno));
                    exit(10);
                }

            }
        }
    }
}

void fileDatagram::analysePacket(int start, int length, char * buffer)
{
    int pckt_number = start / packet_size;

    if(packets[pckt_number].confirmed == false)
    {
        packets[pckt_number].confirmed = true;
        packets[pckt_number].datagram = new char[length];
        packets[pckt_number].datagram_size = length;
        memcpy(packets[pckt_number].datagram, buffer, length);


        packets_ready++;
        float percent = float(packets_ready)/packet_number * 100;

        printf("%.1f %% done\n", percent);
    }

        while(packets[window_iter].confirmed == true && window_iter < packet_number)
        {
            window_iter++;
        }



}

bool fileDatagram::ready()
{
    return (packets_ready == packet_number);
}



void fileDatagram::saveFile()
{
    std::fstream fs;
    fs.open(file_name, std::fstream::out | std::fstream::binary);


    for(int i = 0; i<packet_number; i++)
    {
        for(int j =0; j<packets[i].datagram_size; j++)
            fs<<packets[i].datagram[j];
    }

    fs.close();
}
