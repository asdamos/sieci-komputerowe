//Adam Sawicki
//270814
#ifndef FILEDATAGRAM_HPP
#define FILEDATAGRAM_HPP
#include <cstdio>
#include <ctime>
#include "functions.hpp"

#include <arpa/inet.h>
#include <netinet/ip.h>
#include <fstream>

struct packet
{
    char * datagram;
    int datagram_size;
    bool confirmed;
    struct timeval send_time;
    bool mailed;
};

class fileDatagram
{
    public:
        fileDatagram(char * file_name, int  file_size, int sockfd, int window_size, int packet_size, char * ip_address, int serv_port, int p_t);
        ~fileDatagram();
        void send();
        void analysePacket(int start, int length, char * buffer);
        bool ready();
        void saveFile();

    private:
        char * file_name;
        int file_size;
        int sockfd;
        int window_size;
        int packet_size;
        int packet_timeout;

        int window_iter;

        struct packet * packets;
        int packet_number;  //number of packets
        int packets_ready;
        char ip[20];
        int port;
};

#endif // FILEDATAGRAM_HPP
