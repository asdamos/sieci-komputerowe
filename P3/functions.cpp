//Adam Sawicki
//270814
#include "functions.hpp"

void err_sys(const char* x)
{
    perror(x);
    exit(1);
}


//from lecture examples
void find_ip_adress(char * domain_name, char * ip_address)
{

    struct addrinfo* result;
    struct addrinfo hints;

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_INET;		// chcemy tylko adresy IPv4
    hints.ai_socktype = SOCK_STREAM;	// inaczej dostaniemy trzy struktury (dla SOCK_STREAM, SOCK_DGRAM i SOCK_RAW)

    int error = getaddrinfo(domain_name, NULL, &hints, &result);

    if (error != 0)
    {
        //fprintf(stderr, "getaddrinfo error: %s\n", (error == EAI_SYSTEM) ? strerror(errno) : "other error");
        exit(1);
    }


    for (struct addrinfo* r = result; r != NULL; r = r->ai_next)
    {
        struct sockaddr_in* addr = (struct sockaddr_in*)(r->ai_addr);
        char ip_address2[20];
        inet_ntop (AF_INET, &(addr->sin_addr), ip_address2, sizeof(ip_address2));
        //printf ("%s\n", ip_address);
        strcpy(ip_address, ip_address2);

    }


    freeaddrinfo(result);
}


void Gettimeofday(struct timeval *tv, struct timezone *tz)
{
    if (gettimeofday(tv, tz) == -1)
        err_sys("Gettimeofday error");
    return;
}

u_int64_t time_diff_in_ms(struct timeval start, struct timeval end)
{
    return (end.tv_sec - start.tv_sec) * 1000 + (end.tv_usec - start.tv_usec) / 1000;
}
