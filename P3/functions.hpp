//Adam Sawicki
//270814
#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

#include <cstdio>
#include <cstdlib>
#include <netdb.h>
#include <arpa/inet.h>
#include <cstring>
#include <errno.h>
#include <ctime>
#include <sys/time.h>



void err_sys(const char* x);

void find_ip_adress(char * domain_name, char * ip_address);

void Gettimeofday(struct timeval *tv, struct timezone *tz);

u_int64_t time_diff_in_ms(struct timeval start, struct timeval end);

#endif // FUNCTIONS_HPP
