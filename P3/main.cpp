//Adam Sawicki
//270814
#include <iostream>
#include <cstdio>
#include <arpa/inet.h>
#include <netinet/ip.h>
#include <unistd.h>


#include "functions.hpp"
#include "fileDatagram.hpp"

using namespace std;

int main(int argc, char **argv)
{
    int packet_size = 1000;
    int window_size = 100;
    int packet_timeout = 50;    // in miliseconds

    int port, file_size;
    char * file_name;
    char domain_name[25] = "aisd.ii.uni.wroc.pl";
    char ip_address[20];

    if(argc != 4)
    {
        printf("Program powinien przyjmowac trzy argumenty: port, nazwa pliku wynikowego i rozmiar pliku w bajtach.\n");
        return 0;
    }

    port = atoi(argv[1]);
    file_name = argv[2];
    file_size = atoi(argv[3]);

    find_ip_adress(domain_name, ip_address);


    int sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (sockfd < 0)
    {
        fprintf(stderr, "socket error: %s\n", strerror(errno));
        exit(1);
    }


    struct sockaddr_in server_address;
    bzero (&server_address, sizeof(server_address));
    server_address.sin_family      = AF_INET;
    server_address.sin_port        = htons(port);
    inet_pton(AF_INET, ip_address, &server_address.sin_addr);



    fileDatagram fD(file_name, file_size, sockfd, window_size, packet_size, ip_address,port, packet_timeout);




    struct timeval timeout;
    timeout.tv_sec = 0;
    timeout.tv_usec = 5000;

    while(!fD.ready())
    {
        fD.send();

        fd_set reader;
        FD_ZERO(&reader);
        FD_SET(sockfd, &reader);

        timeout.tv_sec = 0;
        timeout.tv_usec = 5000;

        int n = select(sockfd+1, &reader, NULL, NULL, &timeout);
        if(n>0)
        {
            // Informacja o nadawcy datagramu zwrotnego
            struct sockaddr_in senderAddr;
            socklen_t senderLength = sizeof(senderAddr);

            // Odbieranie datagramu z gniazda

            char buffer[10000];
            int len = recvfrom(sockfd, buffer, 10000, 0, (struct sockaddr*)&senderAddr, &senderLength);
            if(len > 0)
            {
                // Wyciaganie adresu IP i portu nadawcy datagramu zwrotnego
                char senderIP[20];
                inet_ntop(AF_INET, &(senderAddr.sin_addr), senderIP, sizeof(senderIP));
                int senderPort = ntohs(senderAddr.sin_port);

                if(strcmp(senderIP, ip_address) == 0 && senderPort == port)
                {
                    int start;
                    int length;
                    sscanf(buffer, "DATA %d %d\n", &start, &length);

                    char * pch;
                    pch=strchr(buffer, '\n');
                    pch++;
                    fD.analysePacket(start, length, pch);
                }
            }



        }



    }



    fD.saveFile();

    close(sockfd);




    return 0;
}
