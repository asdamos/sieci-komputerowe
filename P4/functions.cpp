//Adam Sawicki
//270814

#include "unp.hpp"
#include <sys/time.h>
#include <string>
#include <sys/stat.h>
#include <fstream>

void Gettimeofday(struct timeval *tv, struct timezone *tz)
{
    if (gettimeofday(tv, tz) == -1)
        err_sys("Gettimeofday error");
    return;
}

u_int64_t time_diff_in_ms(struct timeval start, struct timeval end)
{
    return (end.tv_sec - start.tv_sec) * 1000 + (end.tv_usec - start.tv_usec) / 1000;
}

void send_reply(int fd, char * buff,int iter)
{
    char * buffit = buff;
    char host_name[1000]; host_name[0] = '\0';
    char host_without_port[1000];
    char gpath[1000]; gpath[0] = '\0';
    char path[1000]; path[0] = '\0';
    char conn_type[1000];
    char message[1000000];
    bool get = false, host = false, conn = false;
    int i = 0;
    int version1 = 1, version2 = 1;
    while(buff[i]!='\0')
    {
        if(!get)
        {
            if(sscanf(buffit,"GET %s HTTP/%d.%d\n", gpath, &version1, &version2) == 3)
            {
                get = true;

                //printf("gpath %s\n", gpath);
            }
        }
        if(!host)
        {
            if(sscanf(buffit,"Host: %s\n", host_name) == 1)
            {
                host = true;
                for(unsigned int j=0; j<strlen(host_name); j++)
                {
                    host_without_port[j]=host_name[j];
                    if(host_name[j] == ':')
                    {
                        host_without_port[j]='\0';
                        break;
                    }
                }

                //printf("host_name %s host without port %s\n", host_name, host_without_port);
            }
        }
        if(!conn)
        {
            if(sscanf(buffit,"Connection: %s\n",conn_type) == 1)
            {
                conn = true;
                //printf("conn type %s\n", conn_type);
            }
        }
        while(buff[i] != '\n' && buff[i]!= '\0')
        {
            buffit++;
            i++;
        }
        if(buff[i] == '\n')
        {
            buffit++;
            i++;
        }
    }

    printf("User %d requested site %s%s\n", fd, host_name, gpath);
    printf("Reply code: ");

    if(!get)    //501 not implemented
    {
        sprintf(message, "HTTP/%d.%d 501 Not Implemented\nContent-Type: text/html\nContent-Length: 4\n\n501\n", version1, version2);
        Writen(fd,message, strlen(message));
        printf("501\n");
    }
    else
    {
        //search for ".."
        bool error403 = false;
        for(unsigned int j=1; j<strlen(path); j++)
        {
            if(path[j-1] == '.' && path[j] == '.')
            {
                error403 = true;
                break;
            }
        }
        if(error403)
        {
            sprintf(message, "HTTP/%d.%d 403 Forbidden\nContent-Type: text/html\nContent-Length: 4\n\n403\n", version1, version2);
            Writen(fd,message, strlen(message));
            printf("403\n");
        }
        else
        {
            //200 v 301 v 404

            sprintf(path,"%s/%s%s",folder_path, host_without_port, gpath);
            struct stat s;
            int err = stat(path, &s);
            if(-1 == err)
            {
                if(ENOENT == errno) //404
                {
                    sprintf(message, "HTTP/%d.%d 404 Not Found\nContent-Type: text/html\nContent-Length: 4\n\n404\n", version1, version2);
                    Writen(fd,message, strlen(message));
                    //err_sys("Path does not exists");
                    printf("404\n");
                }
                else
                {
                    err_sys("Stat error");
                }
            }
            else
            {
                if(S_ISDIR(s.st_mode)) //dir - 301
                {
                    if(gpath[strlen(gpath)-1] != '/')
                        sprintf(message, "HTTP/%d.%d 301 Moved Permanently\nLocation: http://%s%s/index.html\nContent-Type: text/html\nContent-Length: 4\n\n404\n", version1, version2, host_name,gpath);
                    else
                        sprintf(message, "HTTP/%d.%d 301 Moved Permanently\nLocation: http://%s%sindex.html\nContent-Type: text/html\nContent-Length: 4\n\n404\n", version1, version2, host_name,gpath);
                    Writen(fd,message, strlen(message));
                    printf("301\n");
                }
                else    //200
                {
                    printf("200\n");
                    char type[1000];
                    int type_it = 0;
                    bool write_type=false;

                    for(unsigned int j = 0; j<strlen(gpath); j++)
                    {
                        if(!write_type)
                        {
                            if(gpath[j] == '.')
                            {
                                write_type=true;
                            }
                        }
                        else
                        {
                            type[type_it] = gpath[j];
                            type_it++;
                        }
                    }
                    type[type_it] = '\0';

                    if(strcmp(type, "txt")==0)
                    {
                        sprintf(message, "HTTP/%d.%d 200 OK\nContent-Type: text/plain; charset=utf-8\nContent-Length: %ld\n\n", version1, version2, s.st_size);
                        Writen(fd,message, strlen(message));
                    }
                    else if(strcmp(type, "html")==0)
                    {
                        sprintf(message, "HTTP/%d.%d 200 OK\nContent-Type: text/html\nContent-Length: %ld\n\n", version1, version2, s.st_size);
                        Writen(fd,message, strlen(message));
                    }
                    else if(strcmp(type, "css")==0)
                    {
                        sprintf(message, "HTTP/%d.%d 200 OK\nContent-Type: text/css; charset=utf-8\nContent-Length: %ld\n\n", version1, version2, s.st_size);
                        Writen(fd,message, strlen(message));
                    }
                    else if(strcmp(type, "jpg")==0)
                    {
                        sprintf(message, "HTTP/%d.%d 200 OK\nContent-Type: image/jpeg\nContent-Length: %ld\n\n", version1, version2, s.st_size);
                        Writen(fd,message, strlen(message));
                    }
                    else if(strcmp(type, "jpeg")==0)
                    {
                        sprintf(message, "HTTP/%d.%d 200 OK\nContent-Type: image/jpeg\nContent-Length: %ld\n\n", version1, version2, s.st_size);
                        Writen(fd,message, strlen(message));
                    }
                    else if(strcmp(type, "png")==0)
                    {
                        sprintf(message, "HTTP/%d.%d 200 OK\nContent-Type: image/png\nContent-Length: %ld\n\n", version1, version2, s.st_size);
                        Writen(fd,message, strlen(message));
                    }
                    else if(strcmp(type, "pdf")==0)
                    {
                        sprintf(message, "HTTP/%d.%d 200 OK\nContent-Type: application/pdf\nContent-Length: %ld\n\n", version1, version2, s.st_size);
                        Writen(fd,message, strlen(message));
                    }
                    else
                    {
                        sprintf(message, "HTTP/%d.%d 200 OK\nContent-Type: application/octet-stream\nContent-Length: %ld\n\n", version1, version2, s.st_size);
                        Writen(fd,message, strlen(message));
                    }

                    //send file
                    char file[s.st_size+5];
                    char tmp;
                    std::fstream fs;
                    fs.open(path, std::fstream::in | std::fstream::binary);
                    unsigned int file_iter;
                    for(file_iter = 0; file_iter<s.st_size; file_iter++)
                    {
                        tmp = fs.get();
                        file[file_iter]=tmp;
                    }
                    file[s.st_size]='\0';

                    Writen(fd,file, s.st_size);
                    fs.close();
                }
            }
        }
    }

    //disconnect
    if(conn)
    {
        if(strcmp(conn_type, "close") == 0)
        {
            printf("User %d disconnected\n", fd);
            shutdown(clients[iter], SHUT_RDWR);
            Close(clients[iter]);
            FD_CLR(clients[iter], &allset);
            clients[iter] = -1;
        }
    }
}
