//Adam Sawicki
//270814
#include <cstdio>
#include <cstring>
#include "unp.hpp"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

using namespace std;

int SERV_PORT;
int clients[FD_SETSIZE];
fd_set      allset;

char * folder_path;

int main(int argc, char **argv)
{
    if(argc != 3)
    {
        err_sys("Wrong argument number");
    }
    else
    {
        SERV_PORT = atoi(argv[1]);
        folder_path = argv[2];
    }

    //check this path
    struct stat s;
    int err = stat(folder_path, &s);
    if(-1 == err)
    {
        if(ENOENT == errno)
        {
            err_sys("Path does not exists");
        }
        else
        {
            err_sys("Stat error");
        }
    }
    else
    {
        if(S_ISDIR(s.st_mode))
        {
            /* it's a dir */
        }
        else
        {
            err_sys("Path is not a directory");
        }
    }


    //server structure
    int         i, maxfd;
    int			listenfd, connfd, sockfd;   //listen socket and connected socket
    int         nready;
    struct timeval clients_time[FD_SETSIZE];
    ssize_t     n;
    fd_set      rset;
    char	    buff[MAXLINE];
    socklen_t   clilen;

    int maxi = -1;          //max index of clients[] array

    struct sockaddr_in	cliaddr, servaddr;

    listenfd = Socket(AF_INET, SOCK_STREAM, 0);

    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family      = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port        = htons(SERV_PORT);


    Bind(listenfd, (SA *) &servaddr, sizeof(servaddr));

    Listen(listenfd, LISTENQ);

    maxfd = listenfd;   //initialize

    printf("Server started on port %d\n", SERV_PORT);

    for (i = 0; i < FD_SETSIZE; i++)
        clients[i] = -1;			// -1 indicates available entry

    FD_ZERO(&allset);   //clear all entries in set
    FD_SET(listenfd, &allset); //add listenfd to allset

    while(true)
    {
        rset = allset;		/* structure assignment */
        struct timeval timeout;
        timeout.tv_sec = 1;
        timeout.tv_usec = 0;
        nready = Select(maxfd+1, &rset, NULL, NULL, &timeout);

        if (FD_ISSET(listenfd, &rset))  	/* new client connection */
        {
            clilen = sizeof(cliaddr);
            connfd = Accept(listenfd, (SA *) &cliaddr, &clilen);

            for (i = 0; i < FD_SETSIZE; i++)
                if (clients[i] < 0)
                {
                    printf("New user connected on socket %d\n", connfd);
                    clients[i] = connfd;	/* save descriptor */
                    Gettimeofday(&clients_time[i], NULL);
                    break;
                }

            if (i == FD_SETSIZE)
                err_sys("too many clients");

            FD_SET(connfd, &allset);	/* add new descriptor to set */
            if (connfd > maxfd)
                maxfd = connfd;			/* for select */
            if (i > maxi)
                maxi = i;				/* max index in client[] array */

            if (--nready <= 0)
                continue;				/* no more readable descriptors */
        }


        for (i = 0; i <= maxi; i++)  	/* check all clients for data */
        {
            if ( (sockfd = clients[i]) < 0)    //not connected
                continue;
            if (FD_ISSET(sockfd, &rset))
            {
                memset(buff, '\0', MAXLINE);
                if ( (n = Read(sockfd, buff, MAXLINE)) == 0)
                {
                    /*connection closed by client */
                    printf("User %d disconnected\n", clients[i]);

                    clients[i] = -1;
                    Close(sockfd);
                    FD_CLR(sockfd, &allset);
                }
                else
                {
                    /*received messages*/
                    Gettimeofday(&clients_time[i], NULL);   //set time of last message
                    send_reply(clients[i], buff, i);
                }
                if (--nready <= 0)
                    break;				/* no more readable descriptors */
            }
        }

        struct timeval now;
        Gettimeofday(&now, NULL);
        u_int64_t time_diff;
        for(i=0; i <= maxi; i++)
        {
            if(clients[i] != -1)
            {
                time_diff = time_diff_in_ms(clients_time[i], now);
                if(time_diff > 1000)
                {
                    printf("User %d timed out\n", clients[i]);
                    shutdown(clients[i], SHUT_RDWR);
                    Close(clients[i]);
                    FD_CLR(clients[i], &allset);
                    clients[i] = -1;
                }
            }
        }
    }

    return 0;
}
