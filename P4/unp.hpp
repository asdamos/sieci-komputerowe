//Adam Sawicki
//270814

/* Our header file */
//using book Unix Network Programming, Vol. 1 by W. Richard Stevens
#ifndef __unp_hpp
#define __unp_hpp

#include <iostream>
#include <sys/socket.h>
#include <sys/types.h>
#include <cstdlib>
#include <strings.h>
#include <netinet/in.h>	/* sockaddr_in{} and other Internet defns */

#include "time.h"
#include <sys/socket.h>
#include <string.h>
#include <cstdio>
#include <unistd.h>
#include <stdarg.h>

#include <errno.h>


using namespace std;

#define	SA	struct sockaddr

#define	LISTENQ		1024	/* 2nd argument to listen() */
#define	MAXLINE		4096	/* max text line length */
#define	MAXSOCKADDR  128	/* max socket address structure size */
#define	BUFFSIZE	8192	/* buffer size for reads and writes */


void err_sys(const char* x); /* error function */

int		 Accept(int, SA *, socklen_t *);
void	 Bind(int, const SA *, socklen_t);
void	 Connect(int, const SA *, socklen_t);
void	 Listen(int, int);
int		 Socket(int, int, int);

void	 Close(int);
void	 Write(int, void *, ssize_t);

int Select(int nfds, fd_set *readfds, fd_set *writefds, fd_set *exceptfds, struct timeval *timeout);

ssize_t Read(int fd, void *ptr, size_t nbytes);

ssize_t writen(int fd, const void *vptr, size_t n); /* Write "n" bytes to a descriptor. */

void Writen(int fd, void *ptr, ssize_t nbytes);


void Gettimeofday(struct timeval *tv, struct timezone *tz);

u_int64_t time_diff_in_ms(struct timeval start, struct timeval end);


void send_reply(int fd, char * buff, int iter);

extern char * folder_path;

extern int clients[FD_SETSIZE];
extern fd_set      allset;

#endif

